import numpy as np
from krokosim.models import *
from krokosim.utils import get_transfere_ratio_per_timestep, add_scenario_factor

from krokosim import a, T, T_Diastole, T_Systole

def init_circuit(session):
    # CompartmentTypes
    ventricle = CompartmentType(
        name='ventricle',
        min_percentage=33,
        max_percentage=100,
        transfer_ratio_per_timestep=get_transfere_ratio_per_timestep(a, T_Systole))
    arteries = CompartmentType(
        name='arteries',
        min_percentage=85,
        max_percentage=115,
        transfer_ratio_per_timestep=get_transfere_ratio_per_timestep(a, T))
    capillaries = CompartmentType(
        name='capillaries',
        min_percentage=75,
        max_percentage=125,
        transfer_ratio_per_timestep=get_transfere_ratio_per_timestep(a, T))
    veins = CompartmentType(
        name='veins',
        min_percentage=65,
        max_percentage=135,
        transfer_ratio_per_timestep=get_transfere_ratio_per_timestep(a, T_Diastole))
    session.add_all([veins, ventricle, arteries, capillaries])

    # Compartments
    ventricle.compartments.append(Compartment(name='rV', volume_start=3.0))
    ventricle.compartments.append(Compartment(name='lV', volume_start=3.0))
    arteries.compartments.append(Compartment(name='left_aorta_prox', volume_start=4.5))
    arteries.compartments.append(Compartment(name='right_aorta_prox', volume_start=4.5))
    arteries.compartments.append(Compartment(name='left_aorta_dist', volume_start=4.5))
    arteries.compartments.append(Compartment(name='right_aorta_dist', volume_start=4.5))
    arteries.compartments.append(Compartment(name='pulm_art', volume_start=2.0))
    capillaries.compartments.append(Compartment(name='digestion', volume_start=4.5))
    capillaries.compartments.append(Compartment(name='ant_body', volume_start=4.0))
    capillaries.compartments.append(Compartment(name='post_body', volume_start=4.5))
    capillaries.compartments.append(Compartment(name='lung', volume_start=3.0))
    veins.compartments.append(Compartment(name='pulm_veins', volume_start=4.0))
    veins.compartments.append(Compartment(name='syst_veins', volume_start=54))

    # CircuitConnections
    # rV -->
    session.add(CircuitConnection(id='a', donor='rV', acceptor='left_aorta_prox',
                                  factor_during_diastole=0, factor_during_systole=0))
    session.add(CircuitConnection(id='b', donor='rV', acceptor='pulm_art',
                                  factor_during_diastole=0, factor_during_systole=1))
    # right_aorta_prox -->
    session.add(CircuitConnection(id='d', donor='right_aorta_prox', acceptor='right_aorta_dist',
                                  factor_during_diastole=0.8, factor_during_systole=0.8))
    session.add(CircuitConnection(id='e', donor='right_aorta_prox', acceptor='left_aorta_prox',
                                  factor_during_diastole=0, factor_during_systole=0))
    session.add(CircuitConnection(id='f', donor='right_aorta_prox', acceptor='ant_body',
                                  factor_during_diastole=0.2, factor_during_systole=0.2))
    # left_aorta_prox -->
    session.add(CircuitConnection(id='g', donor='left_aorta_prox', acceptor='left_aorta_dist',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='h', donor='left_aorta_prox', acceptor='right_aorta_prox',
                                  factor_during_systole=0, factor_during_diastole=0))
    # left_aorta_dist -->
    session.add(CircuitConnection(id='i', donor='left_aorta_dist', acceptor='digestion',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='j', donor='left_aorta_dist', acceptor='right_aorta_dist',
                                  factor_during_diastole=0, factor_during_systole=0))
    # right_aorta_dist
    session.add(CircuitConnection(id='k', donor='right_aorta_dist', acceptor='left_aorta_dist',
                                  factor_during_diastole=0, factor_during_systole=0))
    session.add(CircuitConnection(id='l', donor='right_aorta_dist', acceptor='post_body',
                                  factor_during_diastole=1, factor_during_systole=1))
    # Stable connections:
    session.add(CircuitConnection(id='c', donor='lV', acceptor='right_aorta_prox',
                                  factor_during_diastole=0, factor_during_systole=1))
    session.add(CircuitConnection(id='m', donor='ant_body', acceptor='syst_veins',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='n', donor='post_body', acceptor='syst_veins',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='o', donor='digestion', acceptor='syst_veins',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='p', donor='syst_veins', acceptor='rV',
                                  factor_during_diastole=1, factor_during_systole=0))
    session.add(CircuitConnection(id='q', donor='pulm_art', acceptor='lung',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='r', donor='lung', acceptor='pulm_veins',
                                  factor_during_diastole=1, factor_during_systole=1))
    session.add(CircuitConnection(id='s', donor='pulm_veins', acceptor='lV',
                                  factor_during_diastole=1, factor_during_systole=0))
    session.commit()


def init_test_scenario(session):
    scenario = Scenario(name='default')
    session.add(scenario)
    session.commit()

    add_scenario_factor(
        session, scenario, 'rV', 'left_aorta_prox', 0, 0)

    add_scenario_factor(
        session, scenario, 'right_aorta_prox', 'ant_body', 0, 0)

    add_scenario_factor(
        session, scenario, 'right_aorta_prox', 'left_aorta_prox', 0, 0)

    add_scenario_factor(
        session, scenario, 'left_aorta_prox', 'right_aorta_prox', 0, 0)

    add_scenario_factor(
        session, scenario, 'right_aorta_dist', 'left_aorta_dist', 0, 0)

    add_scenario_factor(
        session, scenario, 'left_aorta_dist', 'right_aorta_dist', 0, 0)

    session.commit()
    return scenario

if __name__ == '__main__':
    from krokosim import session
    Base.metadata.create_all(engine)
    init_circuit(session)
    init_test_scenario(session)
