from krokosim.models import *
from krokosim.simulation import *
from krokosim.utils import *
from krokosim import session, T, T_Diastole, heart_beats

if __name__ == '__main__':

    comps = session.query(Compartment).all()
    num_env = get_initial_num_env(comps)
    ccs = session.query(CircuitConnection).all()
    scenario = session.query(Scenario).filter_by(name='default').one()
    for cc in ccs:
        print cc.id
        session.expunge(cc)
        for scenario_factor in scenario.scenario_factors:
            if scenario_factor.circuit_connection.id == cc.id:
                cc.factor_during_diastole = scenario_factor.circuit_connection.factor_during_diastole
                cc.factor_during_systole = scenario_factor.circuit_connection.factor_during_systole


    # short cuts
    map_CN_i = num_env['map_CN_i']

    if False:
        res = simulation_timestep(ccs, in_diastole=False, **num_env)

    if True:
        for hb in range(heart_beats):
            for t in range(T):
                #print t
                if t < T_Diastole:
                    num_env['in_diastole'] = True
                else:
                    num_env['in_diastole'] = False
                cv_input, cv_output, cv_volume = simulation_timestep(ccs, **num_env)
                num_env['cv_volume'] = cv_volume
                for comp in comps:
                    result = Result(
                        is_diastole = num_env['in_diastole'],
                        scenario_id = scenario.id,
                        heart_beat = hb,
                        timestep = t,
                        compartment_id = comp.id,
                        input = cv_input[map_CN_i[comp.name]],
                        output = cv_output[map_CN_i[comp.name]],
                        volume = cv_volume[map_CN_i[comp.name]],
                        )
                    session.add(result)
            if hb % 50 == 0:
                print "commit session"
                session.commit()

    session.commit()
