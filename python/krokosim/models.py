import sqlalchemy as sa
import sqlalchemy.orm as orm
from sqlalchemy.ext.associationproxy import association_proxy

from krokosim import Base, engine

################
# Krokos Circuit
################

class CompartmentType(Base):
    __tablename__ = 'compartment_types'
    name = sa.Column(sa.String, primary_key=True)
    min_percentage = sa.Column(sa.Integer)
    max_percentage = sa.Column(sa.Integer)
    transfer_ratio_per_timestep = sa.Column(sa.Float)


class Compartment(Base):
    __tablename__ = 'compartments'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, unique=True)
    volume_start = sa.Column(sa.Float)
    compartment_type_name = sa.Column(sa.String, sa.ForeignKey('compartment_types.name'))

    compartment_type = orm.relationship(
        "CompartmentType",
        backref=orm.backref('compartments'))


class CircuitConnection(Base):
    __tablename__ = 'circuit_connections'
    id = sa.Column(sa.String, primary_key=True)
    donor = sa.Column(sa.ForeignKey('compartments.name'))
    acceptor = sa.Column(sa.ForeignKey('compartments.name'))
    factor_during_diastole = sa.Column(sa.Float, default = 1)
    factor_during_systole = sa.Column(sa.Float, default = 1)


###################
# Krokos Scenarios
###################

class Scenario(Base):
    __tablename__ = 'scenarios'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, unique=True, nullable=False)
    shunt = sa.Column(sa.Integer)
    foramen = sa.Column(sa.Integer)
    anastomose = sa.Column(sa.Integer)


class ScenarioFactor(Base):
    __tablename__ = 'scenario_factors'
    circuit_connection_id = sa.Column(sa.ForeignKey('circuit_connections.id'), primary_key=True)
    scenario_id = sa.Column(sa.ForeignKey('scenarios.id'), primary_key=True)
    factor_during_diastole = sa.Column(sa.Float, default = 1)
    factor_during_systole = sa.Column(sa.Float, default = 1)

    circuit_connection = orm.relationship(
        "CircuitConnection",
        backref=orm.backref('scenario_factors'))
    scenario = orm.relationship(
        "Scenario",
        backref=orm.backref('scenario_factors')
    )

Scenario.circuit_connections = association_proxy("scenario_factors", "circuit_connection")
CircuitConnection.scenarios = association_proxy('scenario_factors', "scenario")


def init_scenarios(session):
    default_scenario = Scenario(name='default')
    session.add(default_scenario)


################
# Krokos Results
################

class Result(Base):
    __tablename__ = 'results'
    id = sa.Column(sa.Integer, primary_key=True)
    scenario_id = sa.Column(sa.ForeignKey('scenarios.id'))
    is_diastole = sa.Column(sa.Boolean, nullable=False)
    heart_beat = sa.Column(sa.Integer, nullable=False)
    timestep = sa.Column(sa.Integer, nullable=False)

    compartment_id = sa.Column(sa.ForeignKey('compartments.id'))

    volume = sa.Column(sa.Float)
    input = sa.Column(sa.Float)
    output = sa.Column(sa.Float)

    sa.UniqueConstraint(scenario_id, heart_beat, timestep, compartment_id)


if __name__ == '__main__':
    Base.metadata.create_all(engine)
