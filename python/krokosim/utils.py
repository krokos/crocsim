__author__ = 'philipp'
from models import CircuitConnection, ScenarioFactor

def add_scenario_factor(
        session,
        scenario,
        name_donor,
        name_acceptor,
        factor_during_diastole,
        factor_during_systole):
    cc = session.query(CircuitConnection).\
    filter_by(donor = name_donor, acceptor = name_acceptor).one()
    # session.add(cc)
    # session.commit()
    scenario_factor = ScenarioFactor(
        circuit_connection_id = cc.id,
        scenario_id = scenario.id,
        factor_during_diastole = float(factor_during_diastole),
        factor_during_systole = float(factor_during_systole)
    )
    session.add(scenario_factor)
    #session.commit()
    return scenario_factor

def get_transfere_ratio_per_timestep(a, T):
    result = 1 - a**(1./T)
    return result
