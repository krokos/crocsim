import numpy as np
import sqlalchemy as sa
import sqlalchemy.orm as orm
from sqlalchemy.ext.declarative import declarative_base

#####################
# Database Parameters
#####################

Base = declarative_base()
engine = sa.create_engine('sqlite:///SIMULATION_DB.sqlite', echo=False)

Session = orm.sessionmaker(bind=engine)
session = Session()


#######################
# Simulation Parameters
#######################

T = 100
a = np.float64(0.001)
T_Diastole = 70
T_Systole = T - T_Diastole
heart_beats = 500

