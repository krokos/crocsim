import numpy as np
np.seterr('ignore')

DEBUG=False

def get_initial_num_env(compartments):
    n = len(compartments)
    cv_zeros = np.matrix(np.zeros((n,1)))

    # init variables empty / with zeros
    map_CN_i = {}
    cv_volume = cv_zeros.copy()
    cv_maximum = cv_zeros.copy()
    cv_minimum = cv_zeros.copy()
    cv_transfer_ratio_per_timestep = cv_zeros.copy()
    comp_order = []

    # Fill with values
    for i, comp in enumerate(compartments):
        comp_order.append(comp.name)
        map_CN_i[comp.name] = i
        #print i, comp.volume_start, comp.compartment_type.max_percentage
        cv_volume[i] = comp.volume_start
        #print cv_volume
        cv_maximum[i] = comp.volume_start * comp.compartment_type.max_percentage/100.
        cv_minimum[i] = comp.volume_start * comp.compartment_type.min_percentage/100.
        cv_transfer_ratio_per_timestep[i] = comp.compartment_type.transfer_ratio_per_timestep

    return {'map_CN_i': map_CN_i,
            'cv_zeros': cv_zeros,
            'cv_volume': cv_volume,
            'cv_maximum': cv_maximum,
            'cv_minimum': cv_minimum,
            'cv_transfer_ratio_per_timestep': cv_transfer_ratio_per_timestep,
            'comp_order': comp_order,
            'n': n,
            }

def simulation_timestep(
        circuit_connections,
        map_CN_i, # map (C)ompartment(N)ame to (i)ndex
        cv_zeros,
        cv_volume,
        cv_minimum,
        cv_maximum,
        cv_transfer_ratio_per_timestep,
        comp_order,
        n,
        in_diastole=True):

    m_Distribution = np.matrix(np.zeros((n, n)))
    cv_zeros = cv_volume * 0
    #cv_lims_zero = cv_zeros + 1e-10

    cv_excess = np.multiply(
        np.maximum(cv_zeros, cv_volume - cv_minimum),
        cv_transfer_ratio_per_timestep)
    #cv_capacity = np.maximum(cv_lims_zero, cv_maximum - cv_volume) # PR: warum? Ersetzt durch naechste Zeile
    cv_capacity = np.maximum(cv_zeros, cv_maximum - cv_volume)
    
    if DEBUG: print cv_capacity
    if DEBUG: print m_Distribution
    if in_diastole:
        if DEBUG: print "in diastole"
        factor_label = 'factor_during_diastole'
    else:
        if DEBUG: print "in systole"
        factor_label = 'factor_during_systole'
    for cc in circuit_connections:
        if DEBUG: print getattr(cc, factor_label), cc.donor, '-->', cc.acceptor
        #print 'test'
        m_Distribution[map_CN_i[cc.donor], map_CN_i[cc.acceptor]] = getattr(cc, factor_label)
    if DEBUG: print "**************************************"
    if DEBUG: print m_Distribution
    
    M_1 = (np.multiply(m_Distribution.T, cv_capacity)).T # multiply each column with vector elementwise
    if DEBUG: print "\n M_1: \n"
    if DEBUG: print M_1
    M_2 = np.multiply(M_1, 1./np.sum(M_1, axis=1)) # normalise row-wise
    M_2[np.isnan(M_2)] = 0;
    if DEBUG: print "\n M_2: \n"
    if DEBUG: print M_2
    T_1 = np.multiply(M_2, cv_excess)
    if DEBUG: print "\n T_1: \n"
    if DEBUG: print T_1
    in_p = np.sum(T_1, axis=0) #sum col-wise
    if DEBUG: print "\n in_p:\n", in_p
    in_f = np.minimum(np.multiply(cv_capacity.T, 1./in_p), 1)
    in_f[np.isnan(in_f)] = 0;
    if DEBUG: print np.multiply(cv_capacity.T, 1./in_p)
    if DEBUG: print "\n in_f:\n", in_f
    if DEBUG: print "\n cv_capacityn:\n", cv_capacity
    T_2 = np.multiply(T_1, in_f)
    if DEBUG: print "\n T_2:\n", T_2
    cv_input = np.transpose(np.sum(T_2, axis=0))
    if DEBUG: print cv_input
    cv_output = np.sum(T_2, axis=1)
    if DEBUG: print cv_output
    cv_volume = cv_volume + cv_input - cv_output
    if DEBUG: print cv_volume
    return cv_input, cv_output, cv_volume
