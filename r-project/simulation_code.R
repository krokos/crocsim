#main simulation function
#arguments:
# volume: data frame giving the fill level volumes of the different compartments at different time points, empty data frame if this is a new simulation
# input: data frame with the volumes received by the various compartments in each time step, empty in the case of a new simulation
# output: data frame with the volumes transferred by the various compartments in each time step, empty in the case of a new simulation
# minimum: vector with the minimum fill levels of the various compartments
# maximum: vector with the maximum fill levels of the various compartments
# distribution: matrix of permeabilities
# diastole: vector stating the start and end of diastole
# pvector: vector of proportions by which the excess volumes of the various compartments are reduced in one time step in the absence of input
# num.heartbeats: time span of the simulation

simulate <- function(volumes,input,output,minimum,maximum,permeabilities,diastole,pvector,num.heartbeats=10){ 
  m <- nrow(volumes) #number of time steps from previous calls of the simulate function
  for(i in (m+1):(m+num.heartbeats*100)) {
    if (i %% 100 == 0) {print(i/100)}
    t <- (i-(m+1))%%100+1 #time
    capacity <- sapply(maximum-volumes[i-1,],function(x) max(x,1e-10)) #compute current capacities, the max function prevents volumes from becoming negative due to numerical inaccuracy (see equation 1)
    excess <- sapply(volumes[i-1,]-minimum,function(x) max(x,0))* pvector #compute current excess volumes and multiply by the transfer proportion per time step (see equation 2)
    
    if ((t >= diastole[1]) && (t <= diastole[2])){
        #no blood can leave the heart
        excess["RV"]=0 
        excess["LV"]=0
    }
    else { #no blood can enter the heart
        excess["pulm.veins"]=0
        excess["syst.veins"]=0
        #no flow through Foramen Panizzae
        permeabilities["rAo.prox","lAo.prox"] <- 0
        permeabilities["lAo.prox","rAo.prox"] <- 0
    }
    
    M <- t(t(permeabilities)*capacity) #capacity-weighted permeabilities (equation 3)
    M <- M/rowSums(M) #normalize (equation 4)
    excessmatrix <- matrix(rep(excess,length(excess)),nrow=length(excess)) #generates matrix of excess volumes (all columns equal)
    transfer <- excessmatrix*M	 # the entry in row i and column j states how much compartment i is going to transfer to compartment j in this time step (equation 5)
    
    inputthisstep <- colSums(transfer) # the ith entry states the total volume that other compartments are going to transfer to compartment i in this time step (equation 6)
    inputfaktor<- sapply(capacity/inputthisstep, function(x) min(x,1)) #determines the proportion of the total input that will fit into the various compartments (equation 7)
    transfer <- transfer * matrix(rep(inputfaktor),length(excess),nrow=length(excess),byrow=TRUE) #computes the volumes that will actually be transferred (equation 8)
    #add a new line to the volume, input, and output data frames with the final values for this time step
    input <- rbind(input,colSums(transfer)) #equation 9
    output <- rbind(output,t(rowSums(transfer))) #equation 10
    volumes <- rbind(volumes,volumes[i-1,]-rowSums(transfer)+colSums(transfer)) #equation 11 
  }
  dimnames(volumes) <- list(1:nrow(volumes),dimnames(volumes)[2][[1]])
  return(list(volumes,input,output))
}


#set parameter values (compartment names as in Fig. 1 of the manuscript)

#start volumes = compartment sizes (values from Table 1 of the manuscript)
start <- c(RV=3,LV=3,lAo.prox=4.5,rAo.prox=4.5,lAo.dist=4.5,rAo.dist=4.5,dig=4.5,post.body=4.5,ant.body=4.0,syst.veins=54.0,lung=3,pulm.art=2,pulm.veins=4)


percentages <- c(0,0,15,15,15,15,25,25,25,35,25,15,35)

minimum <- start * (1-percentages/100)
maximum <- start * (1+percentages/100)
#minimum and maximum for the ventricles are treated separately
minimum["RV"]<-1
minimum["LV"]<-1
maximum["RV"]<-3
maximum["LV"]<-3

#start and end of diastole (one heart beat has 100 time steps)
diastole <-c(1,70)

#number of time steps during which the different compartments have a chance to transfer blood
transfertimes <- rep(100,length(minimum))
names(transfertimes) <- names(start)
transfertimes["LV"] <- 100-diastole[2]
transfertimes["RV"] <- 100-diastole[2]
transfertimes["syst.veins"] <- diastole[2]
transfertimes["pulm.veins"] <- diastole[2]

pvector <- 1-(0.001)^(1/transfertimes) #proportion by which the excess volumes are reduced per time step

#permeability of Foramen panizzae and anastomosis
perm.FP <- 0.2
perm.ana <- 0.1
#degree of pulmonary bypass
bypass <- 0.2

#permeability matrix
permeabilities <- as.matrix(outer(minimum,minimum)*0)
#define all non-zero entries
#first the ones that will stay constant
permeabilities["lAo.dist","dig"]<-1
permeabilities["rAo.dist","post.body"]<-1
permeabilities["post.body","syst.veins"] <- 1
permeabilities["ant.body","syst.veins"] <- 1
permeabilities["dig","syst.veins"] <- 1
permeabilities["syst.veins","RV"] <- 1
permeabilities["lung","pulm.veins"] <- 1
permeabilities["pulm.veins","LV"] <- 1
permeabilities["pulm.art","lung"] <- 1
permeabilities["LV","rAo.prox"] <- 1
permeabilities["rAo.prox","rAo.dist"]<-(1-perm.FP)*0.8
permeabilities["rAo.prox","ant.body"]<-(1-perm.FP)*0.2
permeabilities["rAo.prox","lAo.prox"]<-perm.FP
permeabilities["lAo.prox","rAo.prox"]<-perm.FP
permeabilities["lAo.prox","lAo.dist"]<-1-perm.FP
permeabilities["RV","lAo.prox"] <- bypass
permeabilities["RV","pulm.art"] <- 1-bypass
permeabilities["lAo.dist","rAo.dist"]<-perm.ana
permeabilities["rAo.dist","lAo.dist"]<-perm.ana
permeabilities["lAo.dist","dig"]<-1-perm.ana
permeabilities["rAo.dist","post.body"]<-1-perm.ana


#set up data frames for variables
volumes <- matrix(start,nrow=1)
input <- matrix(numeric(length(start)),nrow=1)
output <- matrix(numeric(length(start)),nrow=1)
dimnames(volumes) <- list(1,names(start))
dimnames(input)<- list(1,names(start))
dimnames(output)<- list(1,names(start))

#run the simulation
result <- simulate(volumes,input,output,minimum,maximum,permeabilities,diastole,pvector,num.heartbeats=20)
volumes <- result[[1]]
input <- result[[2]]
output <- result[[3]]

#access fill levels at the end of the simulation
volumes[nrow(volumes),]

#look at how fill levels change over time
matplot(1:nrow(volumes)/100,volumes,type="l",lty=c(rep(1,8),rep(2,5)),col=1:13,xlab="time (in heartbeats)",ylab="fill level",lwd=2,xaxs="i",log="y")
legend(x=20,y=50,col=1:13,lty=c(rep(1,8),rep(2,5)),legend=c("RV","LV","lAo.prox","rAo.prox","lAo.dist","rAo.dist","dig","post.body","ant.body","syst.veins","lung","pulm.art","pulm.veins"),lwd=2,cex=0.75,xjust=1)

