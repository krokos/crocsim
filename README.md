# Python Simulation #

## Dependencies ##

* python-numpy
* python-sqlalchemy

## Example Simulation ##

* Download source code (push the "Downloads"-button and follow the instructions)
* extract the archive-file
* switch to folder **/python/**
* run **python init_circuit.py**
* run **python example_script.py**

## Results ##

The results are stored in the resulting "SIMULATION_DB.sqlite"-file (use Firefox-Add-On "SQLIte Manager")


# R-Code Simulation #

## Example Simulation ##

* Download source code (push the "Downloads"-button and follow the instructions)
* extract the archive-file
* switch to folder **/r-project/**
* run **R**
* run **source ("simulation_code.R")**